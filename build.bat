@echo on

if []==[%1] goto cmd_error
if []==[%2] goto cmd_error

mvn --batch-mode ^
    release:branch ^
    -DautoVersionSubmodules=true ^
    -DupdateBranchVersions=true ^
    -DupdateWorkingCopyVersions=true ^
    -DupdateDependencies=true ^
    -DupdateVersionsToSnapshot=false ^
    -DpushChanges=false ^
    -DremoteTagging=false ^
    -DsuppressCommitBeforeBranch=false ^
    -DbranchName=RELEASE-%1 ^
    -DreleaseVersion=%1 ^
    -DdevelopmentVersion=%2-SNAPSHOT ^
    -DdryRun=true

REM    -Dproject.rel.org.gibbo:gibbo-pom=%1 ^
REM    -Dproject.rel.org.gibbo:child-one=%1 ^
REM    -Dproject.rel.org.gibbo:child-two=%1


:cmd_error

echo You need to supply two version parameters (release version, development version)
goto endof

:endof